# Texturematica

Unreal Engine Automatic Texture-to-Material plugin

<div align="center">
    ![Plugin Screenshot](Readme/screenshot-texturematica-scriptedactions-01.png)
</div>

## About

This Unreal Engine plugin allows for faster iteration of material creation for Unreal Engine workflow. It will automatically generate a master material and material instances from selected static meshes and their corresponding textures.

## Background

This Unreal Engine plugin is part of my proof-of-concept pipeline & tools workflow for automation of the Unreal Engine course [Build a Detective's Office Game Environment](https://dev.epicgames.com/community/learning/courses/WK/unreal-engine-build-a-detective-s-office-game-environment/YOr/unreal-engine-introduction-to-the-detective-s-office-course).

This plugin helps automate the toil of material creation from that course. Though most tasks outlined in the tutorial can be automated in Blueprint, easy material instance (constant) creation does not exist in UE 4.27 Blueprint. So I just implemented all tasks as _BlueprintCallable_ C++ functions.

## Requirements

- [Unreal Engine 4.27+](https://www.unrealengine.com/en-US)

## Installation

This is a C++ plugin. [Add to your project and build](https://dev.to/dolbyio/installing-unreal-engine-plugins-from-github-or-source-code-4dhb).

## Usage

- Create a blueprint that leverages the C++ functions.

| ![Texturematica Blueprint](Readme/screenshot-texturematica-blueprint-01.png) |
|:---:|
| [View on BlueprintUE](https://blueprintue.com/blueprint/qkxnyqff/) |

- Add meshes and textures to a directory structure, as outlined in the course:

```
.
├── Meshes
│   └── [Prop|Structure]
│       └── SM_[Prop|Structure]_MeshName.fbx
└── Textures
    └── [Prop|Structure]
        ├── T_[Prop|Structure]_MeshName_D.tga
        ├── T_[Prop|Structure]_MeshName_M.tga
        └── T_[Prop|Structure]_MeshName_N.tga
```

- Import static mesh(es) into level
- Optional: [Zero meshes to origin](https://blueprintue.com/blueprint/05-7pau9)
- Select static mesh actor(s) in UE editor
- *right-click > Scripted Actions > [Blueprint Name]*

## In Action

<div align="center">
![Plugin Video](https://github.com/JustAddRobots/vault/assets/59129905/81dd5e02-b6f1-4aa7-b371-9e00113823aa)
</div>

## TODO

- [ ] Add automatic semantic versioning
- [ ] Add handling of emissives
- [ ] Add custom parameterized mesh, texture, material directories

## Support

This proof-of-concept is an alpha, thus currently unsupported.

## License

Licensed under GNU GPL v3. See [LICENSE](LICENSE).
